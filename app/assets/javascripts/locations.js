let warehouses = [
  { title: 'Florida', location: 'Clearwater,+FL' },
  { title: 'Michigan', location: '1000+cottage+grove+st+se+grand+rapids+mi+49507' }
];

const url = new URL(window.location.href);

function renderMaps() {
  if (window.location.pathname.includes('/locations/')) {

    // Get JSON data from the page
    var place = $('#location-json').data('location');
    var key = 'AIzaSyBcPZo0uggYBKJR3xXiUBHkywaIJS-YaIE';
    if (place !== null && place !== undefined) {
      initMap(place, key);
    }
  
    // updates rendered output from JSON directions
    populateLocationValues();
  }
}

$(document).on('turbolinks:load', function(){
  if (url.search === '') {
    url.searchParams.set('warehouse', 'Michigan');
    localStorage.setItem('warehouse', 'Michigan');
  }
  listWarehouses();
  renderMaps();
});

$(document).on('turbolinks:load', function(){
  if (window.location.pathname == '/') {
    const warehouse = localStorage.getItem('warehouse');
    $('a.location-link').attr('href', function(i, h) {
      return h + (h.indexOf('?') !== -1 ? `&warehouse=${warehouse}` : `?warehouse=${warehouse}`);
    });
  }
});

function initMap(place, key) {
  if (place.latitude === null && place.longitude === null) {
    alert('Apparently this address did not geocode correctly. Please enter the longitude/latitude coordinates from Google.');
    var container = $('.container-fluid').last();
    var message = 'Please enter the longitude/latitude coordinates from Google. ';
    var alertBox = '<div class="alert alert-warning alert-sticky"><p>' + message + '<a target="_blank" href="http://maps.google.com">Google Maps</a></p></div>';
    $(container).prepend(alertBox).html();
  }
  
  // Place a map by element
  function mapEmbed(element, height, type, parameters) {
    $(element).empty();
    $(element).prepend('<iframe width="100%" height="' + height + '" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/' + type + '?key=' + key + parameters + '"></iframe>');
    // console.log('<iframe width="100%" height="' + height + '" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/' + type + '?key=' + key + parameters + '"></iframe>');
  }
  mapEmbed('#map-satellite', 300, 'view', '&center=' + place.latitude + ',' + place.longitude + '&zoom=17&maptype=satellite');
  mapEmbed('#map-pin', 300, 'place', '&q=' + place.title + '&zoom=12');
  mapEmbed('#map-directions', 400, 'directions', '&origin=' + warehouses.find(wh => wh.title === url.searchParams.get('warehouse')).location + '&destination=' + place.latitude + ',' + place.longitude);
}

function populateLocationValues() {

  var route = {};

  route = {
    distance: directions[0].legs[0].distance.text,
    duration: directions[0].legs[0].duration.text,
    summary: directions[0].summary
  };

  for (const key in route) {
    if (route.hasOwnProperty(key)) {
      const element = route[key];
      $('span#' + key).text(element);
    }
  }

  console.log(route);

}

function watchWarehouseClick() {
  $('.warehouse-link').on('click', function(event) {

    event.preventDefault();
    event.stopPropagation();
    
    let newWh = event.target.dataset.warehouse;
    localStorage.setItem('warehouse', newWh);
    url.searchParams.set('warehouse', newWh);
    
    window.location.href = url.href;
    setTimeout(function() {
      window.location.reload();
    }, 3000);
    
    renderMaps();
    
    listWarehouses();
    $('#warehouses').dropdown('toggle');
  });
}

function listWarehouses() {
  $('#currentWarehouse').text(localStorage.getItem('warehouse'));

  $('#warehouses').html('');
  let html = '';
  warehouses.forEach(wh => {
    html += `<li class="${wh.title === url.searchParams.get('warehouse') ? 'active' : ''} warehouse-link"><a href="#" data-warehouse="${wh.title}">${wh.title}</a></li>`;
  });
  $('#warehouses').html(html);
  
  watchWarehouseClick();
}