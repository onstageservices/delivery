module ApplicationHelper
  
  def page_header(title)
    simple_format(title, {}, wrapper_tag: "div")
  end
  
  def table_classes
    table_classes = 'table table-hover'
  end
  
  def btn_classes_create
    btn_classes_create = 'btn btn-primary'
  end
  
end
