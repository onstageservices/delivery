json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :title, :mpg, :rental_rate, :per_mile_rate
  json.url vehicle_url(vehicle, format: :json)
end
