class VehiclePolicy
  
  attr_reader :current_user, :model
  
  def initialize(current_user, model)
    @current_user = current_user
    @vehicle = model
  end
  
  def index?
    @current_user.user?
  end
  
  def show?
    @current_user.user?
  end
  
  def new?
    @current_user.editor?
  end
  
  def edit?
    @current_user.editor?
  end
  
  def create?
    @current_user.editor?
  end
  
  def update?
    @current_user.editor?
  end
  
  def destroy?
    return false if @current_user == @user
    @current_user.editor?
  end
  
end