class LocationPolicy
  
  attr_reader :current_user, :model
  
  def initialize(current_user, model)
    @current_user = current_user
    @location = model
  end
  
  # def index?
    # @current_user.user?
    # redirect_to (request.referrer || new_user_session_path)
  # end
  
  def show?
    @current_user.user?
  end
  
  def new?
    @current_user.user?
  end
  
  def edit?
    @current_user.user?
  end
  
  def create?
    @current_user.user?
  end
  
  def update?
    @current_user.user?
  end
  
  def destroy?
    return false if @current_user == @user
    @current_user.user?
  end
  
end