# == Schema Information
#
# Table name: locations
#
#  id           :integer          not null, primary key
#  address      :string
#  latitude     :float
#  longitude    :float
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  city         :string
#  state        :string
#  postal_code  :string
#  full_address :string
#  title        :string
#

class Location < ActiveRecord::Base
  validates :title, :presence => true
  validates :address, :presence => true
  
  default_scope { order('title ASC') }
  
  # geocoded_by :address
  # reverse_geocoded_by :latitude, :longitude, :address => :full_address
  # reverse_geocoded_by :latitude, :longitude
  # geocoded_by :full_address
  # after_validation :geocode, :if => :address_changed?
  # after_validation :reverse_geocode, :if => :address_changed?

  geocoded_by :address
  after_validation :geocode, :if => :address_changed?
  reverse_geocoded_by :latitude, :longitude do |obj, results|
    if geo = results.first
      # populate your model
      obj.full_address  = geo.street_address
      obj.city          = geo.city
      obj.state         = geo.state
      obj.postal_code   = geo.postal_code
    end
  end
  # after_validation :reverse_geocode  # auto-fetch address
  after_validation :reverse_geocode, :if => :address_changed?
  # after_validation :fetch_address
  
  # after_validation :geocode, :if => :address_changed?
  
  def street_address
    [full_address, city, state, postal_code].compact.join(', ')
  end
  
end
