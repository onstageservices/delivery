# RailsSettings Model
class Setting < RailsSettings::Base
  cache_prefix { "v1" }

  # Define your fields
  # field :host, type: :string, default: "http://localhost:3000"
  # field :default_locale, default: "en", type: :string
  # field :confirmable_enable, default: "0", type: :boolean
  # field :admin_emails, default: "admin@rubyonrails.org", type: :array
  # field :omniauth_google_client_id, default: (ENV["OMNIAUTH_GOOGLE_CLIENT_ID"] || ""), type: :string, readonly: true
  # field :omniauth_google_client_secret, default: (ENV["OMNIAUTH_GOOGLE_CLIENT_SECRET"] || ""), type: :string, readonly: true
  field :two_day_rate, type: :integer, default: 2
  field :three_day_rate, type: :integer, default: 3
  field :weekly_rate, type: :integer, default: 5
  field :drop_off_pickup_rate, type: :integer, default: 2
  field :delivery_title_one_trip, type: :string, default: "Delivery"
  field :delivery_description_one_trip, type: :string, default: "Vehicle is on site for duration."
  field :delivery_title_two_trips, type: :string, default: "Drop Off & Pick Up"
  field :delivery_description_two_trips, type: :string, default: "Vehicle and driver make two trips to location."
end
