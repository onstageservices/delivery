# == Schema Information
#
# Table name: vehicles
#
#  id            :integer          not null, primary key
#  title         :string           not null
#  mpg           :decimal(8, 2)    not null
#  rental_rate   :decimal(8, 2)    not null
#  per_mile_rate :decimal(8, 2)    not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  fuel_rate     :decimal(8, 2)    not null
#  driver_rate   :decimal(8, 2)    not null
#

class Vehicle < ActiveRecord::Base
  
  validates :title, :presence => true
  validates :mpg, :presence => true
  validates :rental_rate, :presence => true
  validates :per_mile_rate, :presence => true
  validates :fuel_rate, :presence => true
  validates :driver_rate, :presence => true
  
  # def self.total_cost
  #   @total_cost = mpg * rental_rate * per_mile_rate
  # end
  
  # default_scope { order("rental_rate ASC") }
  
end
