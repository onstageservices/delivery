# == Schema Information
#
# Table name: companies
#
#  id              :integer          not null, primary key
#  company_name    :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  company_address :string
#

class Company < ActiveRecord::Base
  # Set up Company to have many_users
  has_many :users
  # We want to validate at least one field you can choose any
  # field you want
  validates :company_name, presence: true
  validates :company_address, presence: true
end
