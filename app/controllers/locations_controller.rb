class LocationsController < ApplicationController
  before_action :set_location, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /locations
  # GET /locations.json
  def index
    
    # @q = Location.ransack(params[:q])
    # @locations = @q.result(distinct: true)
    
    @q = Location.ransack(params[:q])
    @locations = @q.result.paginate(:page => params[:page], :per_page => 10)
    
    @total_locations = Location.all.count
    
    # if params[:q]
    #   @q = Location.ransack(params[:q])
    #   @locations = Location.tagged_with(params[:tag])
    # else
    #   @q = Location.ransack(params[:q])
    #   @locations = @q.result.includes(:artist, :event).paginate(:page => params[:page], :per_page => 10)
    # end
    # authorize Location
  end

  # GET /locations/1
  # GET /locations/1.json
  def show
    @vehicles = Vehicle.all
    @location = Location.find(params[:id])
    @warehouseAddress = '1000 Cottage Grove SE, Grand Rapids, MI 49507'
    if params[:warehouse] == 'Florida'
      @warehouseAddress = 'Clearwater, Florida'
    end
    # authorize @location
    
    gmaps = GoogleMapsService::Client.new
    @routes = gmaps.directions(
      @warehouseAddress,
      @location.try(:address),
      mode: 'driving',
      alternatives: false)
    
    @time = @routes[0][:legs][0][:duration][:value] / 60
    @mileage = @routes[0][:legs][0][:distance][:text].chomp(' mi').to_i
    
    # @street_address = @location.try(:full_address) + ', ' + @location.try(:city) + ', ' + @location.try(:state) + ', ' + @location.try(:postal_code)
  end

  # GET /locations/new
  def new
    @location = Location.new
    @q = Location.ransack(params[:q])
  end

  # GET /locations/1/edit
  def edit
  end

  # POST /locations
  # POST /locations.json
  def create
    @location = Location.new(location_params)
    respond_to do |format|
      if @location.save
        format.html { redirect_to @location, notice: 'Location was successfully created.' }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /locations/1
  # PATCH/PUT /locations/1.json
  def update
    respond_to do |format|
      if @location.update(location_params)
        format.html { redirect_to @location, notice: 'Location was successfully updated.' }
        format.json { render :show, status: :ok, location: @location }
      else
        format.html { render :edit }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.json
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to locations_url, notice: 'Location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_params
      params.require(:location).permit(:title, :address, :city, :state, :postal_code, :latitude, :longitude)
    end
end
