class ApplicationController < ActionController::Base
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  protect_from_forgery with: :exception


  before_filter :set_search

  def set_search
    @q = Location.ransack(params[:q])
  end

  private

    def user_not_authorized
      flash[:alert] = "Access Denied"
      # redirect_to (request.referrer || new_user_session_path)
      redirect_to (new_user_session_path)
    end

end
