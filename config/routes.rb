Rails.application.routes.draw do
  root 'locations#index'
  resources :companies
  resources :locations
  resources :vehicles
  # devise_for :users, :controllers => { registrations: 'registrations'}
  devise_for :users
  resources :users
  namespace :admin do
    resources :settings
  end
end
