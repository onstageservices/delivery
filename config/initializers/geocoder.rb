# config/initializers/geocoder.rb
Geocoder.configure(

  # geocoding service (see below for supported options):
  :lookup => :google,

  # IP address geocoding service (see below for supported options):
  :ip_lookup => :maxmind,

  :maxmind => {:service => :city},

  # to use an API key:
  # :api_key => "AIzaSyCty-xmJ0PHTzGnZ2dW1XKMAigrQPurwng",
  # :api_key => "AIzaSyBT0HtRAL7H0hVvqXIy0F9Am8gT2rDi07I",
  :api_key => "AIzaSyBcPZo0uggYBKJR3xXiUBHkywaIJS-YaIE",

  # geocoding service request timeout, in seconds (default 3):
  :timeout => 5,

  # set default units to kilometers:
  :units => :mi,

  # # caching (see below for details):
  # :cache => Redis.new,
  # :cache_prefix => "...",

  :use_https => true

)
