require 'google_maps_service'

# Setup global parameters
GoogleMapsService.configure do |config|
  config.key = 'AIzaSyBcPZo0uggYBKJR3xXiUBHkywaIJS-YaIE'
  config.retry_timeout = 20
  config.queries_per_second = 10
end