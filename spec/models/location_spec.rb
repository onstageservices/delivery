# == Schema Information
#
# Table name: locations
#
#  id           :integer          not null, primary key
#  address      :string
#  latitude     :float
#  longitude    :float
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  city         :string
#  state        :string
#  postal_code  :string
#  full_address :string
#  title        :string
#

# spec/models/user.rb
require 'rails_helper'

# describe User do
#   it "has a valid factory" do
#     FactoryGirl.create(:user).should be_valid
#   end
#   it "is invalid without an email" do
#     FactoryGirl.build(:user).expect(email).to_not be_nil
#   end
# end

RSpec.describe Location do
  
  it "expects to have a title" do
    is_expected.to validate_presence_of(:title)
  end
  
  it "expects to have an address" do
    is_expected.to validate_presence_of(:address)
  end
  
  describe '#street_address' do
    
    it "expects to have a street address" do
      @location = Location.new(:full_address => '1000 Cottage Grove SE', :city => 'Grand Rapids', :state => 'Michigan', :postal_code => '49507')
      expect(@location.street_address).to eq('1000 Cottage Grove SE, Grand Rapids, Michigan, 49507')
    end
    
  end
  
end
