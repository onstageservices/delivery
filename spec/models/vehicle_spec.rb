# == Schema Information
#
# Table name: vehicles
#
#  id            :integer          not null, primary key
#  title         :string           not null
#  mpg           :decimal(8, 2)    not null
#  rental_rate   :decimal(8, 2)    not null
#  per_mile_rate :decimal(8, 2)    not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  fuel_rate     :decimal(8, 2)    not null
#  driver_rate   :decimal(8, 2)    not null
#

# spec/models/user.rb
require 'rails_helper'

# describe User do
#   it "has a valid factory" do
#     FactoryGirl.create(:user).should be_valid
#   end
#   it "is invalid without an email" do
#     FactoryGirl.build(:user).expect(email).to_not be_nil
#   end
# end

RSpec.describe Vehicle do
  
  it "expects to have a title" do
    is_expected.to validate_presence_of(:title)
  end
  
  it "expects to have an mpg" do
    is_expected.to validate_presence_of(:mpg)
  end
  
  it "expects to have a rental rate" do
    is_expected.to validate_presence_of(:rental_rate)
  end
  
  it "expects to have a per mile rate" do
    is_expected.to validate_presence_of(:per_mile_rate)
  end
  
  it "expects to have a fuel rate" do
    is_expected.to validate_presence_of(:fuel_rate)
  end
  
  it "expects to have a driver rate" do
    is_expected.to validate_presence_of(:driver_rate)
  end
end
