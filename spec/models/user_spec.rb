# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  company_id             :integer
#  role                   :integer
#

# spec/models/user.rb
require 'rails_helper'

# describe User do
#   it "has a valid factory" do
#     FactoryGirl.create(:user).should be_valid
#   end
#   it "is invalid without an email" do
#     FactoryGirl.build(:user).expect(email).to_not be_nil
#   end
# end

RSpec.describe User do
  it "should have an email" do
    is_expected.to validate_presence_of(:email)
  end
end
