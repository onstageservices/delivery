# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  company_id             :integer
#  role                   :integer
#

# spec/factories/users.rb

require 'faker'

# FactoryGirl.define do
#   factory :user do |f|
#     f.email { Faker::Internet.email }
#   end
# end

FactoryGirl.define do
  factory :user do
    # email Faker::Internet.email
    # first_name "John"
    email "email@example.com"
  end
end
