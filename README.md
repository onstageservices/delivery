# Logistics App

## Adding New Users

- Open the console in https://c9.io/onstage/delivery and run the following commands:
- heroku run console
- user = User.new(:email => "email@onstageservices.com", :password => "password", :role => 1)
- user.save
- quit

## Google Maps Platform

[https://console.cloud.google.com/apis/credentials?authuser=1&project=api-project-355500589210](https://console.cloud.google.com/apis/credentials?authuser=1&project=api-project-355500589210)

## Deployment

1. merge changes, pull request
2. pull down changes locally
3. run `bundle exec rake assets:precompile`
4. check in compiled assets to master
5. run `git push heroku master`


## Release Notes

2019-10-16

- updated gems to prepare Heroku stack to update from cedar-14 to heroku-18

2019-12-06

- updated Heroku stack from cedar-14 to heroku-18