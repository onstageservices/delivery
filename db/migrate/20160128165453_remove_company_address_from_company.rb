class RemoveCompanyAddressFromCompany < ActiveRecord::Migration
  def change
    change_column :companies, :company_address, :string, :null => true
  end
end
