class AddFieldsToVehicles < ActiveRecord::Migration
  unless column_exists? :vehicles, :fuel_rate
    add_column :vehicles, :fuel_rate, :decimal
  end
  unless column_exists? :vehicles, :driver_rate
    add_column :vehicles, :driver_rate, :decimal
  end
  def change
    change_column :vehicles, :fuel_rate, :decimal, :precision => 8, :scale => 2, :null => false
    change_column :vehicles, :driver_rate, :decimal, :precision => 8, :scale => 2, :null => false
  end
end
