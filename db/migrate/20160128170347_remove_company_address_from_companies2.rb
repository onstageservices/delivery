class RemoveCompanyAddressFromCompanies2 < ActiveRecord::Migration
  def change
    remove_column :companies, :company_address, :string
  end
end
