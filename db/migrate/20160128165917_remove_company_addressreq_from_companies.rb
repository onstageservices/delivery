class RemoveCompanyAddressreqFromCompanies < ActiveRecord::Migration
  def change
    change_column_null :companies, :company_address, true
  end
end
