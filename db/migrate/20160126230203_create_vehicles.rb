class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :title, :null => false
      t.decimal :mpg, :precision => 8, :scale => 2, :null => false
      t.decimal :rental_rate, :precision => 8, :scale => 2, :null => false
      t.decimal :per_mile_rate, :precision => 8, :scale => 2, :null => false

      t.timestamps null: false
    end
  end
end
