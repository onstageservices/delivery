class RemoveTitleFromLocations < ActiveRecord::Migration
  def change
    remove_column :locations, :title, :string
  end
end
