class RemoveReqtitleFromLocation < ActiveRecord::Migration
  def change
    change_column_null :locations, :address, true
  end
end
