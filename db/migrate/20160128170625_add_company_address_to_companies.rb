class AddCompanyAddressToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :company_address, :string
  end
end
