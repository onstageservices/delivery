class AddOrderToVehicles < ActiveRecord::Migration
  def change
    add_column :vehicles, :order, :integer
  end
end
